# eslint-config

ESLint config for all JS projects

# NPM configuration

1. Configure npm registry to use GitLab packages
```
npm config set @nutrimate:registry https://gitlab.com/api/v4/packages/npm/
npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' "$AUTH_TOKEN"
```

2. Configure authentication to project npm registry to publish the package 
```
npm config set '//gitlab.com/api/v4/projects/23407367/packages/npm/:_authToken' "$AUTH_TOKEN"
```
